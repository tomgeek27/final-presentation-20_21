var baseUrl = 'http://localhost:5000'

function handleBadRequest(res) {
  if(!res.ok) {
    console.error(res)
    var msg = document.getElementById("status-message");
    msg.classList.add("status-error")
    msg.textContent = res.statusText;
  }

  return res;
}

function handleError(res) {
  alert("errore")
}

window.onload = function() {
  fetch(baseUrl + '/todo', {
    method: 'GET'
  })
  .then(handleBadRequest)
  .then((res) => res.json())
  .then((r) => {
    console.log(r)
    const {body} = r
    const todoBody = document.getElementById('todo-body');
    const doneBody = document.getElementById('done-body');
    body.forEach((todo, index) => {
      if (todo.done) {
        // add to done
        const row = document.createElement('tr');
        row.innerHTML = `<td>${todo.text}</td>`;
        doneBody.appendChild(row);
      } else {
        // add to todo table
        const row = document.createElement('tr');
        row.id = `todo-${index}`;
        row.innerHTML = `
          <td scope="row" class="text-left">${todo.text}</td>
          <td>
            <button
              class="btn btn-outline-success btn-sm"
              id=${todo.id}
              onClick="doneTODO(event)"
            >
              Done
            </button>
          </td>`

        todoBody.appendChild(row);
      }
    })
  })
  .catch(handleError)

}

function createTODO() {
  const todo = document.querySelector('input').value;
  var msg = document.getElementById("status-message");

  if(todo == "") {
    msg.classList.add("status-error")
    msg.textContent = "Can't be empty!";
    return;
  }

  fetch(baseUrl + '/todo', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ text: todo, done: false })
  })
  .then(handleBadRequest)
  .then(() => {
    window.location.reload()
  })
  .catch(handleError);
}

function doneTODO(event) {
  const { id } = event.target;

  fetch(baseUrl + `/todo/${id}`, {
    method: 'PATCH',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ done: true })
  })
  .then(handleBadRequest)
  .then(() => window.location.reload())
  .catch(handleError);
}

function resetDB() {

  fetch(baseUrl + `/reset`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ done: true })
  })
  .then(handleBadRequest)
  .then(() => window.location.reload())
  .catch(handleError);
}