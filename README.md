# End-to-end testing

Presentazione finale del corso di Verifica e Convalida A.A. 20/21 di end-to-end testing tramite il tool [Playwright](https://playwright.dev/).

## Demo

### Avvio del server

Spostarsi all'interno della cartella ```demo-app/server```

```shell
cd demo-app/server
```

Scaricare le librerie necessarie tramite il comando:
```shell
yarn
```

Successivamente avviare il server tramite il seguente comando:
```shell
yarn start
```

### Avvio del client

Spostarsi all'interno della cartella ```demo-app/client```

```shell
cd demo-app/client
```

Scaricare il seguente pacchetto npm per l'avvio di un server http:
```shell
npm install -g live-server
```

Successivamente avviare il server tramite il seguente comando:
```shell
live-server --no-browser --port=5500
```

A questo punto sarà possibile accedere alla web-app al seguente link: http://localhost:5500

## Slides

La presentazione si trova all'interno della cartella `slides/`. È possibile visualizzare le slide tramite pagina html (consigliato) oppure tramite il pdf.