package it.unimi.di.tommasoamadori.pageobject.models;

import com.microsoft.playwright.ElementHandle;

import java.util.List;
import java.util.stream.Collectors;

public class DoneList {

    List<DoneElement> doneElements;

    public DoneList(ElementHandle doneElements) {
        this.doneElements = doneElements.querySelectorAll("tr").stream().map(DoneElement::new).collect(Collectors.toList());
    }

    public List<DoneElement> getDoneElements() {
        return doneElements;
    }

    public DoneElement last() {
        return doneElements.get(doneElements.size() - 1);
    }
}
