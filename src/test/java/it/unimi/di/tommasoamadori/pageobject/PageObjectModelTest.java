package it.unimi.di.tommasoamadori.pageobject;

import com.microsoft.playwright.*;
import it.unimi.di.tommasoamadori.pageobject.models.DoneList;
import it.unimi.di.tommasoamadori.pageobject.models.MainPage;
import it.unimi.di.tommasoamadori.pageobject.models.TodoList;
import org.junit.jupiter.api.*;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class PageObjectModelTest {

    private static final String WEB_APP_URL = "http://localhost:5500";
    private static Playwright playwright;
    private static BrowserContext browserContext;
    private static MainPage mainPage;
    private final String RED_COLOR = "rgb(255, 0, 0)";

    @BeforeAll
    public static void setupClass() {
        playwright = Playwright.create();
        BrowserType.LaunchOptions launchOptions = new BrowserType.LaunchOptions();
        launchOptions.setHeadless(false);
        launchOptions.setSlowMo(2000);
        Browser browser = playwright.chromium().launch(launchOptions);
        browserContext = browser.newContext();
    }

    @BeforeEach
    public void setupTest() {
        //Start tracing
        browserContext.tracing().start(new Tracing.StartOptions()
                .setScreenshots(true)
                .setSnapshots(true));

        Page page = browserContext.newPage();
        page.onDialog(this::handleDialog);

//      page.navigate(WEB_APP_URL);
        mainPage = new MainPage(page, WEB_APP_URL);
    }

    @AfterEach
    public void tearDownTest(TestInfo testInfo) {
        mainPage.close();

        //Stop tracing
        browserContext.tracing().stop(new Tracing.StopOptions()
                .setPath(Paths.get("tracePOM-" + testInfo.getDisplayName() + ".zip")));
    }

    @AfterAll
    public static void tearDownClass() {
        playwright.close();
    }

    private void handleDialog(Dialog dialog) {
        assertThat(dialog.message()).doesNotContain("errore");
        dialog.accept();
    }

    @Test
    public void resetDBTest() {
        mainPage.resetDB();

        Assertions.assertAll(
                () ->  assertThat(mainPage.getDoneList()
                        .getDoneElements()) //page.querySelectorAll("#done-body tr");
                        .isEmpty(),
                () ->  assertThat(mainPage.getTodoList()
                        .getTodoElements()) //page.querySelectorAll("#todo-body tr");
                        .isEmpty()
        );
    }

    @Test
    public void invalidInputTest() {

//      page.fill("#newTODO", elementNameTodo);
//      page.click("#create-todo");
        mainPage.createNewTodo("");

        Assertions.assertAll(
                () -> assertThat(mainPage
                        .getStatusMessage().getMessage()) // page.querySelector("#status-message").textContent()
                        .isEqualTo("Can't be empty!"),
                () -> assertThat(mainPage
                        .getComputedStyle( //page.evalOnSelector(..., "(el) => getComputedStyle(el).color\n")
                                mainPage.getStatusMessage(), // page.querySelector("#status-message")
                                "color"))
                        .isEqualTo(RED_COLOR)
        );
    }

    @Test
    public void addItemToTODOListTest() {
        final String elementNameTodo = "NewItemTodo-" + System.currentTimeMillis();
//      page.querySelectorAll("#done-body tr").size();
        final int sizeDoneBeforeCreateTODO = mainPage.getDoneList().getDoneElements().size();

//       page.querySelectorAll("#todo-body tr").size();
        final int sizeTodoBeforeCreateTODO = mainPage.getTodoList().getTodoElements().size();

//      page.fill("#newTODO", elementNameTodo);
//      page.click("#create-todo");
        mainPage.createNewTodo(elementNameTodo);

//      page.querySelectorAll("#todo-body tr");
        TodoList initialTodoList = mainPage.getTodoList();

//      page.querySelectorAll("#done-body tr");
        DoneList initialDoneList = mainPage.getDoneList();

        Assertions.assertAll(
                () -> assertThat(initialTodoList.getTodoElements())
                        .hasSize(sizeTodoBeforeCreateTODO + 1),
                () -> assertThat(initialTodoList
                        .last()
                        .getName()) //...querySelector("td.text-left").textContent()
                        .isEqualTo(elementNameTodo),
                () -> assertThat(initialDoneList.getDoneElements())
                        .hasSize(sizeDoneBeforeCreateTODO)
        );

        initialTodoList
                .last()
                .clickDone(); //...querySelector("td button").click();

//      page.querySelectorAll("#todo-body tr");
        TodoList finalTodoList = mainPage.getTodoList();

//      page.querySelectorAll("#done-body tr");
        DoneList finalDoneList = mainPage.getDoneList();

        Assertions.assertAll(
                () -> assertThat(finalTodoList.getTodoElements())
                        .hasSize(initialTodoList.getTodoElements().size() - 1),
                () -> assertThat(finalDoneList.getDoneElements())
                        .hasSize(sizeDoneBeforeCreateTODO + 1),
                () -> assertThat(finalDoneList
                        .last()
                        .getName()) //...querySelector("td").textContent()
                        .isEqualTo(elementNameTodo)
        );
    }

}
