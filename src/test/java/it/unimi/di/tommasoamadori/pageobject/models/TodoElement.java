package it.unimi.di.tommasoamadori.pageobject.models;

import com.microsoft.playwright.ElementHandle;

public class TodoElement extends WebElement {

    private final String name;
    private final ElementHandle button;

    public TodoElement(ElementHandle todoElement) {
        super(todoElement);
        name = todoElement.querySelector("td.text-left").textContent();
        button = todoElement.querySelector("td button");
    }

    public String getName() {
        return name;
    }

    public void clickDone() {
        button.click();
    }
}
