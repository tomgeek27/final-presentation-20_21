package it.unimi.di.tommasoamadori.pageobject.models;

import com.microsoft.playwright.Page;

public class MainPage {

    private final Page page;

    public MainPage(Page page, String url) {
        this.page = page;
        this.page.navigate(url);
    }

    public DoneList getDoneList() {
        return new DoneList(this.page.querySelector("#done-body"));
    }

    public TodoList getTodoList() {
        return new TodoList(this.page.querySelector("#todo-body"));
    }

    public void createNewTodo(String name) {
        this.page.fill("#newTODO", name);
        this.page.click("#create-todo");
    }

    public StatusMessage getStatusMessage() {
        return new StatusMessage(page.querySelector("#status-message"));
    }

    public void resetDB() {
        this.page.click("#reset-todo");
    }

    public void close() {
        this.page.close();
    }

    public String getComputedStyle(WebElement webElement, String attribute) {
        return (String) page.evaluate("(el) => getComputedStyle(el)." + attribute + "\n", webElement.getElement());
    }

}
