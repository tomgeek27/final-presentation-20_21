package it.unimi.di.tommasoamadori.pageobject.models;

import com.microsoft.playwright.ElementHandle;

import java.util.List;
import java.util.stream.Collectors;

public class TodoList extends WebElement {

    List<TodoElement> todoElements;

    public TodoList(ElementHandle todoElements) {
        super(todoElements);
        this.todoElements = todoElements.querySelectorAll("tr").stream().map(TodoElement::new).collect(Collectors.toList());
    }

    public List<TodoElement> getTodoElements() {
        return todoElements;
    }

    public TodoElement last() {
        return todoElements.get(todoElements.size() - 1);
    }
}
