package it.unimi.di.tommasoamadori.pageobject.models;

import com.microsoft.playwright.ElementHandle;

public abstract class WebElement {

    private final ElementHandle elementHandle;

    public WebElement(ElementHandle elementHandle) {
        this.elementHandle = elementHandle;
    }

    public ElementHandle getElement() {
        return elementHandle;
    }
}
