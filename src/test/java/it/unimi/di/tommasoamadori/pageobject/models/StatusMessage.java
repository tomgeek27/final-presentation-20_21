package it.unimi.di.tommasoamadori.pageobject.models;

import com.microsoft.playwright.ElementHandle;

public class StatusMessage extends WebElement {

    private final String message;

    public StatusMessage(ElementHandle elementHandle) {
        super(elementHandle);
        this.message = elementHandle.textContent();
    }

    public String getMessage() {
        return message;
    }

}
