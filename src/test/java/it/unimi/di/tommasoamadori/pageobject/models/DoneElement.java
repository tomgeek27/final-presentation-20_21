package it.unimi.di.tommasoamadori.pageobject.models;

import com.microsoft.playwright.ElementHandle;

public class DoneElement extends WebElement {
    private final String name;

    public DoneElement(ElementHandle elementHandle) {
        super(elementHandle);
        this.name = elementHandle.textContent();
    }

    public String getName() {
        return name;
    }
}
