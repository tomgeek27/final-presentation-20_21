package it.unimi.di.tommasoamadori.codeless;

import com.microsoft.playwright.*;

public class CodelessTest {
    public static void main(String[] args) {
        try (Playwright playwright = Playwright.create()) {
            Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions()
                    .setHeadless(false));
            BrowserContext context = browser.newContext();
            // Open new page
            Page page = context.newPage();
            // Go to http://localhost:5500/
            page.navigate("http://localhost:5500/");
            // Click input[type="text"]
            page.click("input[type=\"text\"]");
            // Fill input[type="text"]
            page.fill("input[type=\"text\"]", "fdwe");
            // Click text=Create
            // page.waitForNavigation(new Page.WaitForNavigationOptions().setUrl("http://localhost:5500/"), () ->
            page.waitForNavigation(() -> {
                page.click("text=Create");
            });
            // Click text=Reset
            // page.waitForNavigation(new Page.WaitForNavigationOptions().setUrl("http://localhost:5500/"), () ->
            page.waitForNavigation(() -> {
                page.click("text=Reset");
            });
        }
    }

}
