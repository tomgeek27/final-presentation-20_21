package it.unimi.di.tommasoamadori.basic;

import com.microsoft.playwright.*;
import org.junit.jupiter.api.*;

import java.nio.file.Paths;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class BasicTest {

    private static final String WEB_APP_URL = "http://localhost:5500";
    private static Playwright playwright;
    private static BrowserContext browserContext;
    private static Page page;
    private final String RED_COLOR = "rgb(255, 0, 0)";

    @BeforeAll
    public static void setupClass() {
        playwright = Playwright.create();
        BrowserType.LaunchOptions launchOptions = new BrowserType.LaunchOptions();
        launchOptions.setHeadless(false);
        launchOptions.setSlowMo(2000);
        Browser browser = playwright.chromium().launch(launchOptions);
        browserContext = browser.newContext();
    }

    @BeforeEach
    public void setupTest() {
        //Start tracing
        browserContext.tracing().start(new Tracing.StartOptions()
                .setScreenshots(true)
                .setSnapshots(true));

        page = browserContext.newPage();
        page.onDialog(this::handleDialog);
        page.navigate(WEB_APP_URL);
    }

    @AfterEach
    public void tearDownTest(TestInfo testInfo) {
        page.close();

        //Stop tracing
        browserContext.tracing().stop(new Tracing.StopOptions()
                .setPath(Paths.get("traceBASIC-" + testInfo.getDisplayName() + ".zip")));
    }

    @AfterAll
    public static void tearDownClass() {
        playwright.close();
    }

    private void handleDialog(Dialog dialog) {
        assertThat(dialog.message()).doesNotContain("errore");
        dialog.accept();
    }

    @Test
    public void resetDBTest() {
        page.click("#reset-todo");

        List<ElementHandle> doneList = page.querySelectorAll("#done-body tr");
        List<ElementHandle> todoList = page.querySelectorAll("#todo-body tr");

        Assertions.assertAll(
            () -> assertThat(doneList).isEmpty(),
            () -> assertThat(todoList).isEmpty()
        );
    }

    @Test
    public void invalidInputTest() {
        page.click("#create-todo");

        String color = page
                .evalOnSelector(
                        "#status-message",
                        "(el) => getComputedStyle(el).color\n")
                .toString();

        Assertions.assertAll(
                () -> assertThat(page.querySelector("#status-message").textContent())
                        .isEqualTo("Can't be empty!"),
                () -> assertThat(color)
                        .isEqualTo(RED_COLOR)
        );
    }

    @Test
    public void addItemToTODOListTest() {
        final String elementNameTodo = "NewItemTodo-" + System.currentTimeMillis();
        final int sizeDoneBeforeCreateTODO = page.querySelectorAll("#done-body tr").size();
        final int sizeTodoBeforeCreateTODO = page.querySelectorAll("#todo-body tr").size();

        page.fill("#newTODO", elementNameTodo);
        page.click("#create-todo");

        List<ElementHandle> initialTodoList = page.querySelectorAll("#todo-body tr");
        List<ElementHandle> initialDoneList = page.querySelectorAll("#done-body tr");

        Assertions.assertAll(
            () -> assertThat(initialTodoList)
                    .hasSize(sizeTodoBeforeCreateTODO + 1),
            () -> assertThat(initialTodoList.get(initialTodoList.size() - 1).querySelector("td.text-left").textContent())
                    .isEqualTo(elementNameTodo),
            () -> assertThat(initialDoneList)
                    .hasSize(sizeDoneBeforeCreateTODO)
        );

        initialTodoList.get(initialTodoList.size() - 1).querySelector("td button").click();

        List<ElementHandle> finalTodoList = page.querySelectorAll("#todo-body tr");
        List<ElementHandle> finalDoneList = page.querySelectorAll("#done-body tr");

        Assertions.assertAll(
            () -> assertThat(finalTodoList)
                    .hasSize(initialTodoList.size() - 1),
            () -> assertThat(finalDoneList)
                    .hasSize(sizeDoneBeforeCreateTODO + 1),
            () -> assertThat(finalDoneList.get(finalDoneList.size() - 1).querySelector("td").textContent())
                    .isEqualTo(elementNameTodo)
        );
    }
}